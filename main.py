import os
import sys
sys.path.append(os.path.join(os.path.split(os.path.abspath(__file__))[0], 'vendor_tools'))

import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.autoreload
#import os
import time
import socket
import threading
import io
import base64
from Queue import Queue
from tornado.options import define, options, parse_command_line

define("port", default=3000, help="run on the given port", type=int)

os.system('echo starting  NUERA application')
#os.system('/home/root/NUERA_Main &')
#os.environ["RESTART"] = "1"

#print os.environ["RESTART"]

# we gonna store clients in dictionary..
clients = dict()
connection_lock = threading.Lock()
connections = [];
camera_data = Queue()

class XMlPostHandler(tornado.web.RequestHandler):
    def post(self):
        self.set_header("max_age", 1)
        data = self.get_argument('data', '')
        fileName = self.get_argument('fileName', '')
        fileName = fileName[3:]
        try:
            with open(os.path.join(os.path.split(__file__)[0], 'js', fileName), 'w') as fp:
                fp.write(data)
        except:
            pass
        self.write('done')
        #os.system('killall -9 NUERA_Main')
        #os.system('echo Killing done')
	#os.system('sleep 1')
        #os.system('/home/root/NUERA_Main &')
        os.system('echo restarting NUERA application')
	#os.system('/home/root/set.sh')
	
	# Create a TCP/IP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	# Connect the socket to the port where the server is listening
	server_address = ('localhost', 7893)
	print >> sys.stderr, 'connecting to %s port %s' % server_address
	sock.connect(server_address)
	message = "1"
	print len(message)
	sock.sendall(message)
	time.sleep(2)
	print >>sys.stderr, 'closing socket'
	sock.close()

class WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self, *args):
        self.stream.set_nodelay(True)
        with connection_lock:
            connections.append(self)

    def on_message(self, message):
        """
        when we receive some message we want some message handler..
        for this example i will just print message to console
        """
        print "Client %s received a message : %s" % (self.id, message)

    def on_close(self):
        with connection_lock:
            connections.remove(self)

class MyStaticFileHandler(tornado.web.StaticFileHandler):
    def set_extra_headers(self, path):
        # Disable cache
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')

# image_path = os.path.join(os.path.split(__file__)[0], 'image')
js_path = os.path.join(os.path.split(__file__)[0], 'js')
index_path = os.path.split(__file__)[0]
app = tornado.web.Application([
    (r'/websocket', WebSocketHandler),
    (r'/xmldata', XMlPostHandler),
    (r'/js/(.*)', MyStaticFileHandler, {'path': js_path}),
    (r'/?()', tornado.web.StaticFileHandler, {'path':index_path, 'default_filename': 'index.html'})
    # (r'/image/(.*)', tornado.web.StaticFileHandler, {'path': image_path})
])

def socket_read_thread():
    while 1:
#       with open('/home/root/dst_img.jpg', 'rb') as inf:
        with open('C:\Users\Vignesh\Desktop\mvdnuera\image\src_img.jpg', 'rb') as inf:
            encoded_string = base64.b64encode(inf.read())
            camera_data.put(encoded_string)
            time.sleep(0.01)

def client_write_thread():
    while 1:
        try :
            data = camera_data.get(1)
            with connection_lock:
                for connection in connections:
                    connection.write_message('{"type":"cameraUpdate", "data":"%s"}'%data)
        except:
            pass

if __name__ == '__main__':
    parse_command_line()
    app.listen(options.port)
    settings = {
        'debug': True
    }
    # tornado.autoreload = True
    # tornado.autoreload.start()
    # tornado.autoreload.watch("js/sample.xml")
    # tornado.autoreload.watch("js/sampleApp.xml")
    # for dir, _, files in os.walk('js'):
    #     [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]
    threading.Thread(target=client_write_thread).start()
    threading.Thread(target=socket_read_thread).start()
    tornado.ioloop.IOLoop.instance().start()
