/*jslint undef:true*/
var mainApp = angular.module('mainApp');
mainApp.service('socketService', ['cameraDataService', '$rootScope', function (cameraDataService, $rootScope) {
    "use strict";
    /*jslint undef:true*/
    var ws = new WebSocket("ws://" + location.host + "/websocket");
    ws.onopen = function () {
        // Web Socket is connected, send data using send()\
        console.log('open');
    };
    ws.onmessage = function (evt) {
        var receivedMsg = evt.data,
//            headerData = "data:image/jpeg;base64,";
            headerData = "data:image/x-ms-bmp;base64,";
        receivedMsg = JSON.parse(receivedMsg);
        if (receivedMsg.type === 'cameraUpdate') {
            var canvas = document.getElementById("camera-canvas"),
                ctx = canvas.getContext("2d"),
                img = new Image();
            img.onload = function () {
                ctx.drawImage(img, 0, 0);
            };
            img.src = headerData.concat(receivedMsg.data);
        } else {
        }
    };
    ws.onclose = function () {
        console.log('close');
    };
    
}]);
