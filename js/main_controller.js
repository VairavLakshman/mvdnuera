/*jslint undef:true*/
var mainApp = angular.module('mainApp');
mainApp.controller('mainController', ['$scope', '$http', 'cameraDataService', 'socketService', function ($scope, $http, cameraDataService, socketService) {
    'use strict';
    /*jslint undef:true*/
	
		$scope.user = {};
		$scope.password = "";
		$scope.masterPassword = "admin";
		$scope.isLoggedIn = false;
	
		$scope.accordion = {
			current: null
		}
	
		$scope.validateLogin = function()  {
			$scope.showModal = !$scope.showModal;
			if($scope.user.name === "admin" && $scope.user.password === $scope.masterPassword) {
				$scope.isLoggedIn = true;
				$scope.buttonsInfo = [
					{
						"name":"SYSTEM",
						"file":"js/system_configurations_web.xml"
					},
					{
						"name":"APP",
						"file":"js/application_configurations_web.xml"
					},
					{
						"name":"USER CONTROL",
						"file":"js/usercontrol_configurations_web.json"
					},
					{
						"name":"PROFILE",
						"file":"js/profile_configurations_web.json"
					}
				];
			} else {
				$scope.buttonsInfo = [
					{
						"name":"USER CONTROL",
						"file":"js/usercontrol_configurations_web.json"
					}
				];
			}
		}
	
		$scope.selectedName={
				"name":"USER CONTROL",
				"file":"js/usercontrol_configurations_web.json"
			};
	
		$scope.tabSelected = function() {
			var fileName = $scope.selectedName.file;
			var ext = fileName.match(/\.(.+)$/)[1];
			if(angular.lowercase(ext) === 'xml') {
      	$scope.fileName = fileName;
      	$scope.readXml();
			}
			if(angular.lowercase(ext) === 'json') {
				$scope.fileName = fileName;
      	$scope.readJson();
			}
		};
	
		$scope.loginPressed = function() {
			$scope.showModal = !$scope.showModal;
		}
		
		$scope.logOutPressed = function() {
			alert("Logged Out Successfully");
			$scope.isLoggedIn = false;
			$scope.buttonsInfo = [
				{
					"name":"USER CONTROL",
					"file":"js/usercontrol_configurations_web.json"
				}
			];
			$scope.fileName = "js/usercontrol_configurations_web.json";
	
    	$scope.readJson();
		}
	
    $scope.buttonsInfo = [
			{
				"name":"USER CONTROL",
				"file":"js/usercontrol_configurations_web.json"
			}
		];
	
    $scope.cameraDataService = cameraDataService;
	 	
		$scope.readXml = function () {
			$http.get($scope.fileName, {
				transformResponse: function (cnv) {
					var x2js = new X2JS(),
							aftCnv = x2js.xml_str2json(cnv);
					return aftCnv;
				}
			})
			.success(function (response) {
				$scope.data = response;
				if($scope.fileName === "js/system_configurations_web.xml") {
					$scope.systemData = response;
				}
			});
		};
	
		$scope.readJson = function () {
			$http.get($scope.fileName)
			.success(function (data) {
				$scope.data = data;
			});
		};
		
		$scope.buttonPressed = function (id) {
			console.log(id);
		}
		
    $scope.fileName = "js/usercontrol_configurations_web.json";
	
    $scope.readJson();
		
    $scope.showModal = false;
	
    $scope.open = function () {
        $scope.showModal = !$scope.showModal;
    };
	
    $scope.openFile = function(fileName) {
			var ext = fileName.match(/\.(.+)$/)[1];
			if(angular.lowercase(ext) === 'xml') {
      	$scope.fileName = fileName;
      	$scope.readXml();
			}
			if(angular.lowercase(ext) === 'json') {
				$scope.fileName = fileName;
      	$scope.readJson();
			}
    }
		
    $scope.update = function () {
        var js2x = new X2JS(),
            afttransform = new XMLSerializer().serializeToString(js2x.json2xml($scope.data)),
            config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };
        $scope.postRequest = function () {
            return $http.post('/xmldata', $.param({'data': afttransform, 'fileName': $scope.fileName}), config);
        }
        $scope.postRequest()
        .success (function (data) {
          $scope.readXml();
        });
    };
}]);

mainApp.directive('stringToNumber', function () {
    "use strict";
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    };
});

mainApp.directive("ngMin", function () {
    "use strict";
    /*jslint undef:true*/
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModal) {
            var view_value;
            ngModal.$parsers.push(function (value) {
                var return_value,
                    min = scope.$eval(attrs.ngMin);
                if (value < min) {
                    return_value = min;
                    view_value = min;
                    ngModal.$setViewValue(view_value);
                    ngModal.$render();
                } else {
                    return_value = value;
                    view_value = return_value;
                }
                return '' + return_value;
            });
        }
    };
});

mainApp.directive('ngMax', function () {
    "use strict";
    /*jslint undef:true*/
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ngModel) {
            var view_value;
            ngModel.$parsers.push(function (value) {
                var return_value,
                    max = scope.$eval(attr.ngMax);
                if (value > max) {
                    return_value = max;
                    view_value = max;
                    ngModel.$setViewValue(view_value);
                    ngModel.$render();
                } else {
                    return_value = value;
                    view_value = return_value;
                }
                return '' + return_value;
            });
        }
    };
});

mainApp.directive('ngVal', function () {
  "use strict";
  /*jslint undef:true*/
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attr, ngModel) {
      var view_value;
      ngModel.$parsers.push(function (value) {
        var return_value,
            default_ip = scope.$eval(attr.ngVal);
            if(value.match(/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/)) {
              return_value = value;
              view_value = return_value;
            } else {
              return_value = default_ip;
              view_value = default_ip;
              ngModel.$setViewValue(view_value);
              ngModel.$render();
            }
            return return_value;
      });
    }
  };
});

mainApp.directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">Login</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
          scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });

mainApp.$inject = ['$scope'];
